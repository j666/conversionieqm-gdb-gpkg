# -*- coding: utf8 -*-
from __future__ import unicode_literals

import os

try:
    from services.serviceOGR import *
    from services.serviceBdSqlite import *

except:
    from serviceOGR import *
    from serviceBdSqlite import *

# from decorateur import *

class ErreurConvertisseurIEQM(Exception):
    """ JM 2020-12-15
        Classe exception lance lors d'un probleme.
        Exemple apel:   try:
                            ...
                            raise ErreurOGR
                        except ErreurOGR as e:
                            print (e.message)
    """
    def __init__(self, message='Probleme conversion', type='Erreur'):
        self.message = message
        self.type = type

    def __str__(self):
        return repr(self.message)


class ConvertIEQM:

    """ class qui convertie les données IEQM relationelle en geopackage avec ajouts de vues sur peuplement+table relationnelles,
     ajout symbologie default, renomme minuscule

    Exemple:
    # 1-construction objet:
    objConvertIeqm = ConvertIEQM(gdbAConvert="C:/job/gpkg/11n/PRODUITS_IEQM_11N_10.gdb" ,
                                 gpkgNomSortie= "ieqm_11N.gpkg",
                                 pathGpkgStyle= "C:/job/gpkg/ieqm_styles.gpkg",
                                 pathGpkgCode= "C:/job/gpkg/IEQM/tableCodes.gpkg",
                                 # pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
                                 pathGdal="",
                                 nbCaractereAEnleverNomTab=4,
                                 ieqmType="ori", projectionGPKG="4326")


    # 2-lancement conversion

    objConvertIeqm.conversion()

    """
    def __init__(self, gdbAConvert, gpkgNomSortie, pathGpkgStyle, pathGpkgCode,
                 pathGdal="G:\\outilsProdDif\\modules_communs\\gdal\\gdal3.1.3",
                 # pathGdal="G://outilsProdDif/modules_communs/gdal/gdal2.3.2",
                 nbCaractereAEnleverNomTab=4, ieqmType="ori", correctionGeom=False, projectionGPKG= '32198'):

        self.gdbAConvert = gdbAConvert
        self.gpkgNomSortie = gpkgNomSortie
        
        self.pathGpkgStyle = pathGpkgStyle
        self.pathGpkgCode = pathGpkgCode
        self.pathGdal = pathGdal
        self.nbCaractereAEnleverNomTab = nbCaractereAEnleverNomTab
        self.ieqmType = ieqmType
        self.correctionGeom = correctionGeom
        self.projectionGPKG = projectionGPKG

        self.pathSortie = os.path.dirname(self.gdbAConvert)
        self.gpkgSortie = self.pathSortie + "\\" + self.gpkgNomSortie

        self.ogr = ServiceOGR(self.pathGdal)
        self.gdalLocal= True
        if not self.pathGdal:
            self.gdalLocal=False
        self.serviceBd = None

        self.listTable_1_1 = ['climat_pee_ori', 'contraintes_pee_ori', 'stations_for_pee_ori', 'classi_eco_pee_ori', 'meta_ori']
        self.list_table_exclure_vue = ['climat_pee_ori', 'dendro_pee_tiges_dhp_ori','meta_cmp_ori'] 


    def __del__(self):

        del self.ogr
        del self.serviceBd


    def conversion(self):

        try:
            self.effaceVielleBd()
            self.gdb2gpkg()

            self.branchementGpkg()
            self.renomeTableMinuscule()
            self.validErreurGeometrie()
            self.ajoutStyle()
            if self.correctionGeom:
                self.corrigerGeometryInvalide()

            self.ajoutIndex()
            self.ajoutVues()
            self.serviceBd.executeRequete("vacuum")

            self.serviceBd.__del__()

        except Exception as e:
            print(e)
            raise


    def effaceVielleBd(self):
        if os.path.exists(self.gpkgSortie):
            os.remove(self.gpkgSortie)

    # @decorateur_TimeFonction
    def gdb2gpkg(self):

        ## pas de ogc_content car on va créer de nouvelle table avec lettre min tout à l'heure

        # cmdOgr = "ogr2ogr -f gpkg {0} {1} -lco FID=fid -lco GEOMETRY_NAME=geom -t_srs EPSG:4326"\

        cmdOgr = "ogr2ogr -f gpkg {0} {1} -lco FID=fid -lco GEOMETRY_NAME=geom  -t_srs EPSG:{2} -lco spatial_index=no"\
            .format(self.pathSortie +"\\"+ self.gpkgNomSortie, self.gdbAConvert, self.projectionGPKG)


        self.ogr.lancement_cmd(cmdOgr, return_output=False, gdalLocal= self.gdalLocal, igoreError= False)

        # table peup multi - polygon
        # cmdOgr = "ogr2ogr -f gpkg -overwrite  {0} {1} pee_{2}_31P -lco FID=fid -lco GEOMETRY_NAME=geom  -t_srs EPSG:4326 -lco spatial_index=no -nlt POLYGON "\
        #     .format(self.pathSortie +"/"+ self.gpkgNomSortie, self.gdbAConvert, self.ieqmType)

        # self.ogr.lancement_cmd(cmdOgr, return_output=False, gdalLocal= self.gdalLocal, igoreError= False)



    def branchementGpkg(self):

        self.serviceBd = ServiceBdSqlite(self.gpkgSortie, mode_spatialite=True)


    def renomeTableMinuscule(self):

        listTable = self.serviceBd.getListTableBd()


        # pour chaque table, sort la list de champ
        for uneTable in listTable:

            nomTableMinuscule = uneTable.lower()[:-self.nbCaractereAEnleverNomTab]

            #############
            # Creation table avec bonne structure
            ##################

            # ajuste le script pour les tables avec geometry
            geom = False
            if  nomTableMinuscule =='pee_{0}'.format(self.ieqmType) or 'perimetre_no' in nomTableMinuscule:
                sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "geom" MULTIPOLYGON,  '.format(nomTableMinuscule)
                geom = True
            elif 'meta_{0}'.format(self.ieqmType) in nomTableMinuscule:
                sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "geom" POINT, '.format(nomTableMinuscule)
                geom = True
            else:
                sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '.format(nomTableMinuscule)


            listChampsTab= self.serviceBd.getListChampTableNomEtStructure(uneTable)
            stringListChamp = ''

            for unChamp in listChampsTab:
                if unChamp[1] in ('fid', 'geom'):
                    stringListChamp += unChamp[1] + ", "
                    continue

                sqlCreateTable += '"{0}" {1}, '.format(unChamp[1].lower(), unChamp[2], unChamp[3])
                stringListChamp += unChamp[1] + ", "

            sqlCreateTable = sqlCreateTable[:-2]
            sqlCreateTable += ' )'
            stringListChamp = stringListChamp[:-2]

            self.serviceBd.executeRequete(sqlCreateTable, commit=True)

            ##################
            # insert les données dedans la nouvelle table
            ##################
            sqlInsert = "insert into {0} select {1} from {2}".format(nomTableMinuscule, stringListChamp, uneTable)
            self.serviceBd.executeRequete(sqlInsert, commit=True)

            ##############
            # gpkg_contents
            ############
            """ insert into gpkg_contents select 'tes2', data_type, 'tes2', description, last_change, min_x, min_y, max_x, max_y, srs_id 
                from gpkg_contents where table_name = 'pee_ori'
            """

            sqlInsertContents = """ insert into gpkg_contents select '{0}', data_type, '{0}', description, last_change, min_x, min_y, max_x, max_y, srs_id " \
                                "from gpkg_contents where table_name = "{1}" """.format(nomTableMinuscule, uneTable)
            self.serviceBd.executeRequete(sqlInsertContents, commit=True)

            ##############
            # ajoute a gpkg_geometry_columns et delete la vielle
            #############
            """ insert into gpkg_geometry_columns 
                select 'test', column_name, geometry_type_name, srs_id, z, m 
                from gpkg_geometry_columns where table_name = 'pee_ori'
            """

            if geom:
                sqlInsertGeometry = """ insert into gpkg_geometry_columns select '{0}', column_name, geometry_type_name, srs_id, z, m " \
                                     "from gpkg_geometry_columns where table_name = "{1}" """.format(nomTableMinuscule, uneTable)
                self.serviceBd.executeRequete(sqlInsertGeometry, commit=True)

                sqlDeleteGeometry = """ delete from gpkg_geometry_columns where table_name = "{0}" """.format(uneTable)
                self.serviceBd.executeRequete(sqlDeleteGeometry, commit=True)

                # sqlDeleteExt = " delete from gpkg_extensions where table_name = '{}'".format(uneTable)
                # self.serviceBd.executeRequete(sqlDeleteExt, commit=True)

            #############
            # delete vielle
            #############
            sqlDeleteContents = """ delete from gpkg_contents where table_name = "{0}" """.format(uneTable)
            self.serviceBd.executeRequete(sqlDeleteContents, commit=True)

            sqlDropTab = ' drop table "{}"'.format(uneTable)
            self.serviceBd.executeRequete(sqlDropTab, commit=True)

            # drop spatial  index (pas crée via requete OGR -lco spatial_index=no
            # if geom:
                # sql = 'drop table "rtree_{}_geometry"'.format(uneTable)
                # self.serviceBd.executeRequete(sql, commit=True)
                # passe pas au bureau -> no module rtree...


            ######################
            # gpkg_ogr_contents (nb enregistrements)
            ####################
            # renomme le nom table
            sql = "update gpkg_ogr_contents set table_name = '{0}'  where table_name = '{1}' ".format(nomTableMinuscule, uneTable)
            self.serviceBd.executeRequete(sql, commit=True)



    def ajoutStyle(self):
        if self.pathGpkgStyle:
            cmdOgr = """ogr2ogr -f gpkg {0} -append {1} -nln layer_styles -sql "SELECT * from layer_styles" """.format(self.gpkgSortie, self.pathGpkgStyle)
            self.ogr.lancement_cmd(cmdOgr, gdalLocal=self.gdalLocal)

    def ajoutIndex(self):

        listTable = self.serviceBd.getListTableBd()
        ### idx geocode important sans ceci ou sur un autre champ le temps d'ouverture de la bd prend 4:30 minutes
        nomGeocode = 'geocode'
        if self.ieqmType == 'maj':
            nomGeocode = 'geoc_maj'

        for uneTable in listTable:
            if uneTable not in ('meta_cmp_ori', 'layer_styles', 'perimetre_no_terri'):
                sql = " create index idx_geoc_{0} on {0}({1})".format(uneTable, nomGeocode)
                self.serviceBd.executeRequete(sql)

        ### idx spatial pee_ori

        # ogrCmd = """ ogrinfo {0} -sql "select gpkgAddSpatialIndex('pee_ori', 'geom') """.format(self.gpkgSortie) ## passe pas

        ogrCmd = """ogrinfo {0} -sql "select CreateSpatialIndex('pee_{1}', 'geom') " """.format(self.gpkgSortie, self.ieqmType)
        self.ogr.lancement_cmd(ogrCmd, gdalLocal= self.gdalLocal)

        #
        # sql = "select gpkgAddSpatialIndex('pee_ori', 'geom')"
        # self.serviceBd.executeRequete(sql)



    def ajoutVues(self):
        """create view pee_classi_eco
            as select a.fid  as OGC_FID,, a.geom, a.geocode, a.origine, a.an_origine, a.perturb, a.an_perturb, a.type_couv, a.gr_ess, a.cl_dens, a.cl_haut, a.cl_age ,
                b.zone_veg, b.szone_veg, b.dom_bio, b.sdom_bio, b.reg_eco, b.sreg_eco, b.upays_reg, b.dis_eco
            from pee_ori a
            left join classi_eco_pee_ori b
            on a.geocode = b.geocode

            ******* le fid doit absolument s'apeller OGC_FID -> en maj sinon il y a un problème dans l'affichage QGIS...

        """
        listTable = self.serviceBd.getListTableBd()
        for uneTable in listTable:
            if uneTable not in ('pee_ori', 'pee_maj', 'perimetre_no_terri') and uneTable not in self.list_table_exclure_vue:
            # if uneTable not in ('pee_ori', 'pee_maj', 'perimetre_no_terri',  'meta_cmp_ori') and 'dendro_pee_tiges_dhp' not in uneTable:
                nomVue = "vue_peup_{}".format(uneTable)
                listChamp = self.serviceBd.getListchampTable(uneTable)

                if uneTable in self.listTable_1_1 :
                    print('vue cree sur table 1-1 {}'.format(uneTable))

                    sqlCreateView = "create view {0} as select a.fid as OGC_FID, a.geom, a.geocode, a.origine, a.an_origine, a.perturb, a.an_perturb, a.type_couv, a.gr_ess, a.cl_dens," \
                                    " a.cl_haut, a.cl_age, a.part_str, a.etagement, a.et_domi, a.couv_gaule, a.cl_pent, a.dep_sur, a.cl_drai, a.type_eco, a.co_ter, a.type_ter, a.strate," \
                                    "a.met_at_str, a.no_prg, a.ver_prg, a.superficie, ".format(nomVue)

                    for unChamp in listChamp:
                        if unChamp in ('fid', 'geocode', 'geom'):
                            continue

                        sqlCreateView += "b.{}, ".format(unChamp)

                    sqlCreateView = sqlCreateView[:-2]
                    sqlCreateView += " from pee_{0} a left join {1} b on a.geocode = b.geocode".format(self.ieqmType, uneTable )


                elif 'vgpkg_' not in uneTable :
                    print( 'vue cree sur table 1-n :{}'.format(uneTable))
                    # join table 1-n
                    # on inverse le left join (right join non supporté sqlite)
                    sqlCreateView = "create view {0} as select a.fid as OGC_FID, a.geocode, b.geom, ".format(nomVue)
                    for unChamp in listChamp:
                        if unChamp in ('fid', 'geocode'):
                            continue

                        sqlCreateView += "a.{}, ".format(unChamp)

                    # JM 202108- retrait du champ CO_TER. Dans CO_TER il n'y a pas de photo-interprétation, comme toute les tables 1-n sont 
                    # relatives au arb/essence il est impossible d'avoir une valeur de CO_TER noté dans ces dernière. Donc champ CO_TER inutile et meme amenant
                    #  de la confusion.
                    sqlCreateView += " b.origine, b.an_origine, b.perturb, b.an_perturb, b.type_couv, b.gr_ess, b.cl_dens, b.cl_haut, b.cl_age, b.part_str, " \
                                     "b.etagement, b.et_domi, b.couv_gaule, b.cl_pent, b.dep_sur, b.cl_drai, b.type_eco, b.type_ter, b.strate," \
                                     "b.met_at_str, b.no_prg, b.ver_prg, b.superficie " \
                                     "from {0} a " \
                                     "left join pee_{1} b " \
                                     "on a.geocode = b.geocode""".format(uneTable, self.ieqmType)
                else:
                    # table virtuel gpkg
                    continue


                ###########
                # on lance
                ##########
                self.serviceBd.executeRequete(sqlCreateView, commit=True)

                #########
                # ajoute content + geometry

                sqlInsertContents = """ insert into gpkg_contents select '{0}', data_type, '{0}', description, last_change, min_x, min_y, max_x, max_y, srs_id " \
                                    "from gpkg_contents where table_name = 'pee_{1}' """.format(nomVue, self.ieqmType)
                self.serviceBd.executeRequete(sqlInsertContents, commit=True)

                sqlInsertGeometry = """ insert into gpkg_geometry_columns select '{0}', column_name, geometry_type_name, srs_id, z, m " \
                                     "from gpkg_geometry_columns where table_name = 'pee_{1}' """.format(nomVue, self.ieqmType)
                self.serviceBd.executeRequete(sqlInsertGeometry, commit=True)

    def corrigerGeometryInvalide(self):
        print('correction geom')
        # result = self.serviceBd.ST_getCountGeomProbleme("pee_ori", "geom" )
        self.serviceBd.ST_corrigerGeomNotValid("pee_{0}".format(self.ieqmType), "geom")


    def validErreurGeometrie(self):

        listErreurGeometrie = self.serviceBd.ST_getCountGeomProbleme_allTable()

        if len(listErreurGeometrie)> 0:
            self.serviceBd.ferme_connexionBD()
            os.remove(self.gpkgSortie)
            raise ErreurConvertisseurIEQM('Traitement interompu!! Certaines couches dans la GDB contiennent des erreurs de géométries, veuillez corriger\n {}'
                                          .format(listErreurGeometrie))



if __name__ == '__main__':

    # path = "E:/GPKG/carte_ori/jean/"

    # list_a_convert = ["PRODUITS_IEQM_31J_10_C.gdb", "PRODUITS_IEQM_31K_10_C.gdb", "PRODUITS_IEQM_31M_10_C.gdb", "PRODUITS_IEQM_31N_10_C.gdb",
    #                   "PRODUITS_IEQM_31O_10_C.gdb", "PRODUITS_IEQM_31P_10_C.gdb", "PRODUITS_IEQM_32C_10_C.gdb", "PRODUITS_IEQM_32D_10_C.gdb"]

    # for i in list_a_convert:
    #     gdbAConvert = path+i
    #     gpkgNomSortie = i[:-3] + "gpkg"
    #     objConvertIeqm = ConvertIEQM(gdbAConvert = gdbAConvert,
    #                                  gpkgNomSortie = gpkgNomSortie,
    #                                  pathGpkgStyle="G:/OutilsProdDIF/modules_communs/python27/conversionFormat/prerequis/ieqm_styles.gpkg",
    #                                  pathGpkgCode="",
    #                                  pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
    #                                  # pathGdal="",
    #                                  nbCaractereAEnleverNomTab=4, correctionGeom=False)
    #     objConvertIeqm.conversion()

    # objConvertIeqm = ConvertIEQM(gdbAConvert="E:/GPKG/maj/CARTE_ECO_MAJ_UG_111.gdb" ,
    #                              gpkgNomSortie= "ieqm_majug111_repair.gpkg",
    #                              pathGpkgStyle= "E:/GPKG/styles/ieqm_styles.gpkg" ,
    #                              pathGpkgCode= "",
    #                              pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
    #                              nbCaractereAEnleverNomTab=4,
    #                              ieqmType="maj", correctionGeom=True)

    # objConvertIeqm = ConvertIEQM(gdbAConvert="E:/GPKG/maj/CARTE_ECO_MAJ_PROV.gdb",
    #                              gpkgNomSortie="ieqm_maj_prov.gpkg",
    #                              pathGpkgStyle="E:/GPKG/styles/ieqm_styles.gpkg",
    #                              pathGpkgCode="",
    #                              pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
    #                              nbCaractereAEnleverNomTab=5,
    #                              ieqmType="maj")

    # objConvertIeqm = ConvertIEQM(gdbAConvert="G:/Fdif/Transit/coulo3/gpkg/PRODUITS_IEQM_01_10josee.gdb" ,
    #                              gpkgNomSortie= "ieqm_01_10_test.gpkg",
    #                              pathGpkgStyle= "G:/OutilsProdDIF/modules_communs/python27/conversionFormat/prerequis/ieqm_styles.gpkg" ,
    #                              pathGpkgCode= "",
    #                              pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
    #                              nbCaractereAEnleverNomTab=7 )

    objConvertIeqm = ConvertIEQM(gdbAConvert="C:/job/gpkg/11n/PRODUITS_IEQM_11N_10.gdb",
                                 gpkgNomSortie="ieqm_11N_tesnew.gpkg",
                                 pathGpkgStyle="",
                                #  pathGpkgStyle="G:/OutilsProdDIF/modules_communs/python27/conversionFormat/prerequis/ieqm_styles.gpkg",
                                 pathGpkgCode="",
                                #  pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
                                 pathGdal="",
                                 nbCaractereAEnleverNomTab=4,  correctionGeom=False, projectionGPKG="32198"
                                 )
    # objConvertIeqm = ConvertIEQM(gdbAConvert="E:/GPKG/carte_ori/31K/PRODUITS_IEQM_31K_10_5NC.gdb",
    #                              gpkgNomSortie="ieqm_31K_ori_repairNewStyle2.gpkg",
    #                              pathGpkgStyle="G:/OutilsProdDIF/modules_communs/python27/conversionFormat/prerequis/ieqm_styles.gpkg",
    #                              pathGpkgCode="",
    #                              pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
    #                              # pathGdal="",
    #                              nbCaractereAEnleverNomTab=4,  correctionGeom=False
    #                              )

    # objConvertIeqm = ConvertIEQM(gdbAConvert="C:/job/gpkg/11n/PRODUITS_IEQM_11N_10.gdb",
    #                              gpkgNomSortie="ieqm_11N.gpkg",
    #                              pathGpkgStyle="C:/job/GPKG/ieqm_styles.gpkg",
    #                              pathGpkgCode="",
    #                              pathGdal="",
    #                              # pathGdal="",
    #                              nbCaractereAEnleverNomTab=4,correctionGeom=True)

    # objConvertIeqm = ConvertIEQM(gdbAConvert="C:/job/gpkg/21e/PRODUITS_IEQM_21E_10.gdb",
    #                              gpkgNomSortie="ieqm_21e.gpkg",
    #                              pathGpkgStyle="C:/job/GPKG/ieqm_styles.gpkg",
    #                              pathGpkgCode="",
    #                              pathGdal="",
    #                              nbCaractereAEnleverNomTab=4, correctionGeom=True)


    objConvertIeqm.conversion()

    print('FIN')

# ServiceBdSqlite(self.path_fichier_bd_sqlite)