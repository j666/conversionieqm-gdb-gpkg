

### JM 2022 lancement de CMD ogr standalone dans python

import subprocess
from subprocess import CalledProcessError


listPathShp = ['D:/a_temp/SHP/uaPrevu_Originale.shp', 'D:/a_temp/SHP/uaPrevu_Simple1.shp']
pathGpkg =  'D:/a_temp/ensemble.gpkg'
pathSqlite =  'D:/a_temp/ensemble.sqlite'

for shp in listPathShp:
    cmdOgr = 'ogr2ogr -f gpkg {0} {1} -nln coucheEnsemble  -append '.format(pathGpkg, shp)
    # cmdOgr = 'ogr2ogr -f sqlite {0} {1} -nln coucheEnsemble -append -dsco spatialite=yes'.format(pathSqlite, shp)   ## pour une bd .sqlite -spatialite plutot que .gpkg
    try:
        output = subprocess.check_output(cmdOgr, stdin=None, stderr=subprocess.STDOUT, shell=True)
    except Exception as e:
        print('probleme', e)


print('fin on aura les 2 shp dans la meme couche')
## attention par contre comme on met les 2 shp dans la meme table ils doivent avoir la meme structure
# sinon il manquera des info ou ca va planter