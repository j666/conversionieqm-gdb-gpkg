# -*- coding: utf8 -*-
from __future__ import unicode_literals


# try:
#     from Tkinter import Tk, Label, Button, Entry, N, S, E, W, IntVar, Radiobutton, END, HORIZONTAL, DISABLED, ACTIVE, Toplevel, RAISED, StringVar
#     # from tkFileDialog import askopenfilename, askdirectory
#     import tkFileDialog
#     import tkMessageBox
#     print('import py 2')
#     pythonVersion = 2
#
# except:
from tkinter import Tk, Label, Button, Entry, N, S, E, W, IntVar, Radiobutton,Checkbutton, END, HORIZONTAL, DISABLED, ACTIVE, Toplevel, RAISED, StringVar, IntVar
from tkinter import filedialog as tkFileDialog
from tkinter import messagebox as tkMessageBox

# print("import py3")
# pythonVersion = 3

# from tkinter import Tk, Button, Label, Entry, E, W, END, filedialog, messagebox, Radiobutton, IntVar, END, ACTIVE, DISABLED, StringVar, RAISED, HORIZONTAL
# from tkinter.filedialog import askopenfilename, askdirectory
import threading
from tkinter.ttk import Progressbar
from functools import partial
import time
from class_convertIEQM import *
from class_convertGPKG import *

import os, sys
import os.path


class App_conv_gpkg(Tk):
    # Application developpé pour convertir en gpkg et appliquer les standards: lettre minuscule partout, creation de vue, 4326, symbologie par default.
    # Jo Martel 2021-06
    # 1.3 2022-06 ajout possibilite de pointe un dossier contenant plusieurs gdb
    # 1.4 2022-07 valide erreur de geometry car avec erreur la conversion est mauvaise

    def __init__(self):

        Tk.__init__(self)

        self.version = 1.4
        self.geometry("1000x460+400+200")
        self.title("Conversion en Geopackage. Version: {}".format(self.version))
        # self.arboSmullin = None


        def hide_me(event):
            print('hide')
            event.widget.pack_forget()

        # Widget

        self.type_conversion = StringVar()
        self.type_conversion.set("gpkg")

        self.manyGdb = IntVar()
        # self.manyGdb.set(False)
        # self.manyGdb.set(False)


        self.verification = False
        self.erreur = False
        # self.type_app.set(1)
        # self.v = IntVar()
        # self.v.set(1)
        # self.type_conversion = [("ieqm", "ieqm"), ("gpkg", "gpkg")]
        #
        # self.label1 = Label(self, text="Le repertoire de départ est: Smullin/public")
        # self.label1.grid(row=0,column=0, columnspan=2, padx=10, sticky=W)
        #
        # self.label2 = Label(self, text="Le repertoire de déplacement est : ... ")
        # self.label2.grid(row=1,column=0, padx=10, sticky=W)

        self.labelMessage = Label(self, text="La conversion sera faite selon standards gpkg (voir documentation pour détail)\n\n "
                                             "Avant de convertir veuillez vous assurer que les couches dans la GDB ne contiennent\n pas d'erreur de géometrie selon le standard OGC (GEOS)", fg="blue", font=("Helvetica", 11))
        self.labelMessage.grid(row=0,column=1, padx=100, pady=10, sticky=W)


        self.radio_gpkg = Radiobutton(self, text="GPKG", variable= self.type_conversion, value="gpkg", command=self.choix_conversion)
        self.radio_ieqm = Radiobutton(self, text="GPKG_IEQM", variable=self.type_conversion, value="ieqm", command=self.choix_conversion)
        # self.radio_gpkg.bind('btn1', hide_me)


        self.radio_gpkg.grid(row=2, columnspan=2,padx=230,pady=20, sticky=W)
        self.radio_ieqm.grid(row=2, columnspan=2, padx=450, pady=20,  sticky=W)

        self.labelProj = Label(self, text="Projection du GPKG")
        self.labelProj.grid(row=3, column=0, padx=10, pady=10)

        self.boxProj = Entry(self, width=15)
        self.boxProj.insert(0, "32198")
        self.boxProj.grid(row=3, column=1, padx=30, pady=10, sticky=W)

        self.check_manyGdb = Checkbutton(self, text="plusieurs GDB dans le dossier", variable=self.manyGdb, onvalue=1, offvalue=0)
        self.check_manyGdb.grid(row=3, columnspan=2, padx=100, pady=20, sticky=E)

        self.gdbAConvert = Button(self, text="GDB à convertir", command=partial(self.jopen, "directorie", "boxGdbAConvert"), width=20)
        self.gdbAConvert.grid(row=4, column=0, padx=10, pady=10)
        #
        self.boxGdbAConvert = Entry(self, width=125)
        self.boxGdbAConvert.grid(row=4, column=1, padx=30, pady=10)

        self.pathBdStyle = Button(self, text="Gpkg contenant les \n symbologies à inclure", command=partial(self.jopen, "fil", "boxPathStyle"), width=20)
        self.pathBdStyle.grid(row=5, column=0, padx=15, pady=10, sticky=W)
        # self.pathBdStyle.grid_remove()

        self.boxPathStyle = Entry(self, width=125)
        self.boxPathStyle.grid(row=5, column=1, padx=30, pady=10)
        # self.boxPathStyle.insert(0, "G:/OutilsProdDIF/modules_communs/python27/conversionFormat/prerequis/ieqm_styles.gpkg")
        # self.boxPathStyle.grid_remove()

        self.labelNbCarac =  Label(self, text="Nb. caractère à retirer \ndu nom des tables dans la bd")
        self.labelNbCarac.grid(row=6, column=0, padx=10, pady=10)
        self.labelNbCarac.grid_remove()

        self.boxNbCarac = Entry(self, width=5)
        self.boxNbCarac.insert(0, 4)
        self.boxNbCarac.grid(row=6, column=1, sticky=W, padx=30, pady=10)
        self.boxNbCarac.grid_remove()


        self.bt_exe = Button(self, text="Convertir", command=self.executer, fg="red", font="Verdana 10 bold", height=2, width=9,relief=RAISED)
        self.bt_fer = Button(self, text="Fermer", command=self.destroy, height=2, width=6, relief=RAISED )

        self.bt_exe.grid(row=8, column=1, sticky=W, padx=140, pady=10)
        self.bt_fer.grid(row=8, column=1, sticky=W, padx=300, pady=30)


        self.progress = Progressbar(self, orient=HORIZONTAL, length=500, mode='determinate', value=0)
        # self.progress = Progressbar(self, orient=HORIZONTAL, length=200, mode='determinate', maximum=100, variable=self.prc_rendu, value=0)


        # attribut
        self.gpkgSortie = ""
        self.convertisseur = None
        self.listGdbAConvertir = []


        # self.mainloop()
    def changeButtonState(self, state):
        # state choice disabled or normal
        self.radio_gpkg['state']=state
        self.radio_ieqm['state']=state
        self.gdbAConvert['state']=state
        self.boxGdbAConvert['state']=state
        self.pathBdStyle['state']=state
        self.boxPathStyle['state']=state
        self.boxNbCarac['state']=state
        self.bt_exe['state']=state
        self.boxProj['state']=state
        self.check_manyGdb['state']=state


    def choix_conversion(self):
        print('hide')
        if self.type_conversion.get() == 'gpkg':
            # self.pathBdStyle.grid_remove()
            # self.boxPathStyle.grid_remove()
            self.labelNbCarac.grid_remove()
            self.boxNbCarac.grid_remove()
            self.boxPathStyle.delete(0, END)

        else:

            # self.boxPathStyle.grid()
            # self.pathBdStyle.grid()
            self.labelNbCarac.grid()
            self.boxNbCarac.grid()
            self.boxPathStyle.insert(0, "G:/OutilsProdDIF/modules_communs/python27/conversionFormat/prerequis/ieqm_styles.gpkg")


    def jopen(self, option, nomBox):

        repNom = ""

        if option == "fil":
            repNom = tkFileDialog.askopenfilename(title="Pointer le fichier ", filetypes=[('Geopackage', '*.gpkg')])
        else:
            repNom = tkFileDialog.askdirectory(title="Indiquer le repertoire")

        leBox = getattr(self, nomBox)

        leBox.delete(0, END)
        leBox.insert(0, repNom)


    def checkPathGdb(self):
        gdb = self.boxGdbAConvert.get()
        if not gdb:
            tkMessageBox.showerror("Arret du traitement", "GDB à convertir est vide")
            # return False
            raise Exception
        if self.manyGdb.get() == 1:
            return

        if gdb[-4:].lower() != '.gdb':
            tkMessageBox.showerror("Arret du traitement", "La gdb indiquée n'est pas valide {}".format(gdb))
            # return False
            raise Exception

        return

    def getListGdb(self, pathDossier):
        """ donnera la list des dossiers présent dans le repertoire indique"""

        listDirFile = os.listdir(pathDossier)
        listGdb = []
        for i in listDirFile:
            if os.path.isdir(pathDossier+'/'+i) and i[-4:].lower() == '.gdb':
                listGdb.append(pathDossier+'/'+i)

        if len(listGdb) ==0:
            tkMessageBox.showerror("Arret du traitement", "Le repertoire indiqué ne contient aucune GDB")
            raise Exception("le repertoire contenant plusieurs GDB n'en contient aucunes")
        return listGdb

    def executer(self):

        def real_traitement():
            self.erreur = False
            self.progress.grid(row=1, column=1, padx=30, sticky=W)
            # https: // stackoverflow.com / questions / 7310511 / how - to - create - downloading - progress - bar - in -ttk
            # ** a progress window
            try:
                self.checkPathGdb()
                # ok = self.checkPathGdb()

            # if ok:
                ## 2022-06 ajout plusieurs gdb dans le rep
                if self.manyGdb.get() == 1:
                    repManyGdb = self.boxGdbAConvert.get()
                    self.listGdbAConvertir = self.getListGdb(repManyGdb)
                    print('ici')
                else:
                    self.listGdbAConvertir.append(self.boxGdbAConvert.get())
                cptGdb = 0
                for uneGdb in self.listGdbAConvertir:

                    self.gpkgSortie = uneGdb[:-3] + 'gpkg'
                    nomSortie = os.path.basename(uneGdb)[:-3] +'gpkg'
                    gdbAConvert = uneGdb

                    # self.gpkgSortie = self.boxGdbAConvert.get()[:-3] + 'gpkg'
                    # nomSortie = os.path.basename(self.boxGdbAConvert.get())[:-3] +'gpkg'
                    # gdbAConvert = self.boxGdbAConvert.get()
                    bdStyle = self.boxPathStyle.get()
                    scriptPath = os.path.dirname(os.path.realpath(sys.argv[0]))

                    # gdalPath = "E:/GPKG/git/conversionGPKG/code/gdalLocal"
                    gdalPath = os.path.dirname(os.path.dirname(scriptPath))+ "\\gdalLocal"
                    # print('gdalPath1 = {}'.format(gdalPath))
                    # print(gdalPath)
                    if not os.path.exists(gdalPath):
                        gdalPath = os.path.dirname(scriptPath) + "\\gdalLocal"
                        # print('gdalPath2 = {}'.format(gdalPath))
                        if not os.path.exists(gdalPath):
                            gdalPath = scriptPath + "\\gdalLocal"
                            # print('gdalPath3 = {}'.format(gdalPath))
                    gdalPath = os.path.normpath(gdalPath)
                    os.environ['PROJ_LIB'] = gdalPath+'\\projlib'

                    # print('gdalPath final = {}'.format(gdalPath))
                    if self.type_conversion.get() == 'gpkg':

                        self.convertisseur = ConvertisseurGPKG(gdbAConvert=gdbAConvert,
                                                        gpkgNomSortie= nomSortie,
                                                        pathGpkgStyle= bdStyle,
                                                        pathGdal=gdalPath,
                                                        projectionGPKG=self.boxProj.get()
                                                        )
                    else:
                        self.convertisseur = ConvertIEQM(gdbAConvert= gdbAConvert,
                                                gpkgNomSortie= nomSortie,
                                                pathGpkgStyle= bdStyle,
                                                pathGpkgCode="",
                                                pathGdal=gdalPath,
                                                # pathGdal="",
                                                nbCaractereAEnleverNomTab=int(self.boxNbCarac.get()),
                                                correctionGeom=False,
                                                projectionGPKG=self.boxProj.get()
                                                )

                    # convertisseur.conversion()
                
                    try:
                        cptGdb += 1
                        self.labelMessage['text'] = 'GDB {0}/{1}: Conversion en gpkg, veuillez patienter...'.format(cptGdb, len(self.listGdbAConvertir))
                        self.progress['value'] = 5
                        self.convertisseur.effaceVielleBd()
                        self.convertisseur.gdb2gpkg()
                        self.convertisseur.branchementGpkg()

                        self.labelMessage['text'] = 'GDB {0}/{1}: Renommer table et champ en minuscule, veuillez patienter...'.format(cptGdb, len(self.listGdbAConvertir))
                        self.progress['value'] = 40
                        try:
                            self.convertisseur.getDicTabGeom()
                        except:
                            pass

                        self.convertisseur.renomeTableMinuscule()
                        self.progress['value'] = 55

                        self.labelMessage['text'] = 'GDB {0}/{1}: Validation erreur géométrie, veuillez patienter...'.format(cptGdb, len(self.listGdbAConvertir))
                        self.convertisseur.validErreurGeometrie()
                        self.progress['value'] = 75


                        self.labelMessage['text'] = 'GDB {0}/{1}: Ajout index et symbologie, veuillez patienter...'.format(cptGdb, len(self.listGdbAConvertir))
                        self.progress['value'] = 85

                        self.convertisseur.ajoutIndex()
                        try:
                            if self.boxPathStyle.get():
                                self.convertisseur.ajoutStyle()
                        except:
                            tkMessageBox.showwarning("Style par default", "L'outil a rencontré un probleme: Il ne trouve pas le GPKG de style: {}."
                            "Aucun style par default n'a été inclus dans le gpkg".format(self.boxPathStyle.get()))
                        if self.type_conversion.get() == 'ieqm':
                            self.labelMessage['text'] = 'Ajout vues, veuillez patienter...'
                            self.progress['value'] = 90
                            self.convertisseur.ajoutVues()


                        self.labelMessage['text'] = 'GDB {0}/{1}: Ménage BD (vacuum), veuillez patienter...'.format(cptGdb, len(self.listGdbAConvertir))
                        self.progress['value'] = 95
                        self.convertisseur.serviceBd.executeRequete("vacuum")


                        self.convertisseur.serviceBd.__del__()



                    except Exception as e:
                        print(e)
                        if "main thread is not in main loop" in e.__str__():
                            tkMessageBox.showerror("Arret du traitement", "Comme le traitement a été intérompu le gpkg ici {}, sera corrompu. \nVous devez l'effacer!".format(self.gpkgSortie))

                        else:
                            tkMessageBox.showerror("Problème", "GDB {0}/{1}: L'outil à rencontré un problème:\n {2}".format(cptGdb, len(self.listGdbAConvertir), e))
                            self.labelMessage['text'] = "L'outil à rencontré un probleme. Traitement non terminé"
                            self.progress.stop()
                            self.progress.grid_forget()
                            self.changeButtonState('disabled')
                            # self.radio_gpkg['state']='normal'
                            # self.radio_ieqm['state']='normal'
                            # self.gdbAConvert['state']='normal'
                            # self.boxGdbAConvert['state']='normal'
                            # self.pathBdStyle['state']='normal'
                            # self.boxPathStyle['state']='normal'
                            # self.boxNbCarac['state']='normal'
                            # self.bt_exe['state']='normal'
                            # self.bt_exe['state']='normal'
                            # self.boxProj['state']='normal'
                        raise
            except Exception as e:
                # tkMessageBox.showerror("Problème", e)
                self.labelMessage['text'] = "L'outil à rencontré un problème. Traitement non terminé"
                self.progress.stop()
                self.progress.grid_forget()
                self.changeButtonState('normal')
                self.erreur = True


            # self.labelMessage['text'] = 'Conversion en gpkg, veuillez patienter'
            # self.progress['value'] = 5
            # print('lance la verif')
            # time.sleep(5)
            #
            # self.progress['value'] = 25
            # # self.update_idletasks()
            # print('temp 1')
            # time.sleep(5)
            #
            # self.progress['value'] = 75
            # # self.update_idletasks()
            # print('temp 1')
            # time.sleep(5)

            print('fin')
            if self.erreur:
                return

            self.progress.stop()
            self.progress.grid_forget()
            # if ok:

            self.labelMessage['text'] = 'Conversion terminée avec succes!!.\nLe gpkg est au meme endroit que la gdb'
            self.changeButtonState('normal')
            # self.radio_gpkg['state']='normal'
            # self.radio_ieqm['state']='normal'
            # self.gdbAConvert['state']='normal'
            # self.boxGdbAConvert['state']='normal'
            # self.pathBdStyle['state']='normal'
            # self.boxPathStyle['state']='normal'
            # self.boxNbCarac['state']='normal'
            # self.bt_exe['state']='normal'
            # self.bt_exe['state']='normal'
            # self.boxProj['state']='normal'
            # self.bt_fer['state']='normal'

        self.changeButtonState('disabled')
        # self.radio_gpkg['state']='disabled'
        # self.radio_ieqm['state']='disabled'
        # self.gdbAConvert['state']='disabled'
        # self.boxGdbAConvert['state']='disabled'
        # self.pathBdStyle['state']='disabled'
        # self.boxPathStyle['state']='disabled'
        # self.boxNbCarac['state']='disabled'
        # self.bt_exe['state']='disabled'
        # self.boxProj['state']='disabled'
        # self.bt_fer['state']='disabled'

        threading.Thread(target=real_traitement).start()


if __name__ == '__main__':
    app = App_conv_gpkg()
    app.mainloop()