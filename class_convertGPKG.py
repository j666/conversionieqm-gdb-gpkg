# -*- coding: utf8 -*-
from __future__ import unicode_literals

import os
try:
    from services.serviceOGR import *
    from services.serviceBdSqlite import *

except:
    from serviceOGR import *
    from serviceBdSqlite import *

# from decorateur import *

class ErreurConvertisseurGPKG(Exception):
    """ JM 2020-12-15
        Classe exception lance lors d'un probleme.
        Exemple apel:   try:
                            ...
                            raise ErreurOGR
                        except ErreurOGR as e:
                            print (e.message)
    """
    def __init__(self, message='Probleme conversion', type='Erreur'):
        self.message = message
        self.type = type

    def __str__(self):
        return repr(self.message)


class ConvertisseurGPKG:

    """ class qui convertie les données en GPKG 
     ajout symbologie default, renomme minuscule

    Exemple:
    # 1-construction objet:
    convertisseur = ConvertisseurGPKG(gdbAConvert="E:/GPKG/carte_ori/11N/PRODUITS_IEQM_11N_10.gdb",
                                 gpkgNomSortie="ieqm_11n_idem.gpkg",
                                 pathGpkgStyle="",
                                 pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2", projectionGPKG="4326"
                 )


    # 2-lancement conversion

    convertisseur.conversion()

    """
    def __init__(self, gdbAConvert, gpkgNomSortie, pathGpkgStyle,
                 pathGdal="G:\\outilsProdDif\\modules_communs\\gdal\\gdal3.1.3", projectionGPKG='32198'):
                 # pathGdal="E:\\GPKG\\git\\conversionGPKG\\gdalLocal", projectionGPKG='32198'):
                #  pathGdal="G://outilsProdDif/modules_communs/gdal/gdal2.3.2"):

        self.gdbAConvert = gdbAConvert
        self.gpkgNomSortie = gpkgNomSortie
        
        self.pathGpkgStyle = pathGpkgStyle
        self.pathGdal = pathGdal

        self.pathSortie = os.path.dirname(self.gdbAConvert)
        self.gpkgSortie = self.pathSortie + "\\" + self.gpkgNomSortie

        self.ogr = ServiceOGR(self.pathGdal)
        self.gdalLocal= True
        if not self.pathGdal:
            self.gdalLocal=False
        self.projectionGPKG = projectionGPKG
        self.serviceBd = None
        self.dicTabGeom = None


    def __del__(self):

        del self.ogr
        del self.serviceBd


    def conversion(self):

        try:
            self.effaceVielleBd()
            self.gdb2gpkg()

            self.branchementGpkg()
            self.getDicTabGeom()

            self.renomeTableMinuscule()
            self.validErreurGeometrie()
            if self.pathGpkgStyle:
                self.ajoutStyle()
            self.ajoutIndex()
            self.serviceBd.executeRequete("vacuum")

            self.serviceBd.__del__()
            print('FIN conversion!!!')

        except Exception as e:
            print(e)
            raise


    def effaceVielleBd(self):
        if os.path.exists(self.gpkgSortie):
            os.remove(self.gpkgSortie)

    # @decorateur_TimeFonction
    def gdb2gpkg(self):
        # cmdOgr = "ogr2ogr -f gpkg {0} {1} -lco FID=fid -lco GEOMETRY_NAME=geom -t_srs EPSG:4326"\

        cmdOgr = "ogr2ogr -f gpkg {0} {1} -lco FID=fid -lco GEOMETRY_NAME=geom  -t_srs EPSG:{2} -lco spatial_index=no"\
            .format(self.pathSortie +"\\"+ self.gpkgNomSortie, self.gdbAConvert, self.projectionGPKG)
        self.ogr.lancement_cmd(cmdOgr, return_output=False, gdalLocal= self.gdalLocal, igoreError= False)



    def branchementGpkg(self):

        self.serviceBd = ServiceBdSqlite(self.gpkgSortie, mode_spatialite=True)
        # self.serviceBd = ServiceBdSqlite(self.gpkgSortie, mode_spatialite=False)

    def getDicTabGeom(self):

        self.dicTabGeom = self.serviceBd.getDicTabGeom()

        # sql = """ select  "table_name", "column_name", "geometry_type_name", "srs_id" from  "gpkg_geometry_columns" """
        # self.serviceBd.executeRequete(sqlRequete=sql)
        # result = self.serviceBd.leCursor.fetchall()
        ## dic = {'PERIMETRE_NO_TERRI_11N': ['geom', 'MULTIPOLYGON', 4326], 
        #           'PEE_ORI_11N': ['geom', 'MULTIPOLYGON', 4326], 
        #           'META_ORI_11N': ['geom', 'POINT', 4326]
        #       }
        # self.dicTabGeom = self.serviceBd.list_tup2dic(list_tup=result, key_index_tup=0, value_index_tup_deb=1,value_index_tup_fin=3)


    def renomeTableMinuscule(self):

        listTable = self.serviceBd.getListTableBd()


        # pour chaque table, sort la list de champ
        for uneTable in listTable:

            nomTableMinuscule_tmp = uneTable.lower()+'_tmp'
            nomTableMinuscule = uneTable.lower()
          
            #############
            # Creation table _tmp avec bonne structure
            ##################

            # ajuste le script pour les tables avec geometry
            geom = False
            if uneTable in list(self.dicTabGeom.keys()):
                # print('trouve')
                if self.dicTabGeom[uneTable][1] == 'MULTIPOLYGON':
                    sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "geom" MULTIPOLYGON,  '.format(nomTableMinuscule_tmp)
                    geom = True
                elif self.dicTabGeom[uneTable][1] == 'POINT':
                    sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "geom" POINT, '.format(nomTableMinuscule_tmp)
                    geom = True
                elif self.dicTabGeom[uneTable][1] == 'POLYGON':
                    sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "geom" POLYGON, '.format(nomTableMinuscule_tmp)
                    geom = True
                elif self.dicTabGeom[uneTable][1] == 'LINESTRING':
                    sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "geom" LINESTRING, '.format(nomTableMinuscule_tmp)
                    geom = True
                elif self.dicTabGeom[uneTable][1] == 'MULTILINESTRING':
                    sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "geom" MULTILINESTRING, '.format(nomTableMinuscule_tmp)
                    geom = True
                else:
                    raise ErreurConvertisseurGPKG('le type de geometry de la table {} est inconnue'.format(uneTable))
            else:
                sqlCreateTable = 'create table {0} ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '.format(nomTableMinuscule_tmp)


            listChampsTab= self.serviceBd.getListChampTableNomEtStructure(uneTable)
            stringListChamp = ''

            for unChamp in listChampsTab:
                if unChamp[1] in ('fid', 'geom'):
                    stringListChamp += unChamp[1] + ", "
                    continue

                sqlCreateTable += '"{0}" {1}, '.format(unChamp[1].lower(), unChamp[2], unChamp[3])
                stringListChamp += unChamp[1] + ", "

            sqlCreateTable = sqlCreateTable[:-2]
            sqlCreateTable += ' )'
            stringListChamp = stringListChamp[:-2]

            ## 'create table productivite_pee_ori_11n_tmp ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "geocode" TEXT(21), "essence" TEXT(3), "iqs_pot" REAL, "ic_iqs_inf" REAL, "ic_iqs_sup" REAL, "accrst_pot" REAL, "ic_ast_inf" REAL, "ic_ast_sup" REAL )'
            self.serviceBd.executeRequete(sqlCreateTable, commit=True)

            ##################
            # insert les données dedans la nouvelle table _tmp
            ##################
            ## 'insert into productivite_pee_ori_11n_tmp select fid, GEOCODE, ESSENCE, IQS_POT, IC_IQS_INF, IC_IQS_SUP, ACCRST_POT, IC_AST_INF, IC_AST_SUP from PRODUCTIVITE_PEE_ORI_11N'
            
            sqlInsert = "insert into {0} select {1} from {2}".format(nomTableMinuscule_tmp, stringListChamp, uneTable)
            self.serviceBd.executeRequete(sqlInsert, commit=True)


            ##############
            # gpkg_contents
            ############
            """ " update gpkg_contents set table_name = 'classi_eco_pe', identifier = 'classi_eco_pe' where table_name = 'classi_eco_pe' "
            """

            sqlUpdate = """ update gpkg_contents set table_name = '{0}', identifier = '{0}' where table_name = '{1}' """.format(nomTableMinuscule, uneTable)
            self.serviceBd.executeRequete(sqlUpdate, commit=True)
            
            ##############
            # ajoute a gpkg_geometry_columns
            #############
            """ insert into gpkg_geometry_columns 
                select 'test', column_name, geometry_type_name, srs_id, z, m 
                from gpkg_geometry_columns where table_name = 'pee_ori'
            """

            if geom: 
                sqlUpdate = """ update gpkg_geometry_columns set table_name = '{0}' where table_name = '{1}' """.format(nomTableMinuscule, uneTable)
                self.serviceBd.executeRequete(sqlUpdate, commit=True)

            #############
            # delete vielle tab
            #############

            # sqlDeleteContents = """ delete from gpkg_contents where table_name = "{0}" """.format(uneTable)
            # self.serviceBd.executeRequete(sqlDeleteContents, commit=True)

            sqlDropTab = ' drop table "{}"'.format(uneTable)
            self.serviceBd.executeRequete(sqlDropTab, commit=True)

            #############
            # renomme table sans _tmp
            ######################

            sqlRenommeTab = "ALTER TABLE {0} RENAME TO '{1}' ".format(nomTableMinuscule_tmp, nomTableMinuscule)
            self.serviceBd.executeRequete(sqlRenommeTab, commit=True)


            ######################
            # gpkg_ogr_contents (nb enregistrements)
            ####################
            # renomme le nom table
            # if self.nbCaractereAEnleverNomTab != 0:
            #     sql = "update gpkg_ogr_contents set table_name = '{0}'  where table_name = '{1}' ".format(nomTableMinuscule, uneTable)
            #     self.serviceBd.executeRequete(sql, commit=True)


    def ajoutStyle(self):
        ## 'ogr2ogr -f gpkg C:/job/gpkg/11n/idem_11N.gpkg -append E:/GPKG/styles/ieqm_styles.gpkg -nln layer_styles -sql "SELECT * from layer_styles" '

        cmdOgr = """ogr2ogr -f gpkg {0} -append {1} -nln layer_styles -sql "SELECT * from layer_styles" """.format(self.gpkgSortie, self.pathGpkgStyle)
        self.ogr.lancement_cmd(cmdOgr, gdalLocal=self.gdalLocal)


    def ajoutIndex(self):
       ## 'ogrinfo C:/job/gpkg/11n/idem_11N.gpkg -sql "select CreateSpatialIndex(\'perimetre_no_terri_11n\', \'geom\') " '
        for cle, val in self.dicTabGeom.items():

            ogrCmd = """ogrinfo {0} -sql "select CreateSpatialIndex('{1}', '{2}') " """.format(self.gpkgSortie, cle.lower(), val[0])
            self.ogr.lancement_cmd(ogrCmd, gdalLocal= self.gdalLocal)



    def validErreurGeometrie(self):

        listErreurGeometrie = self.serviceBd.ST_getCountGeomProbleme_allTable()

        if len(listErreurGeometrie)> 0:
            self.serviceBd.ferme_connexionBD()
            os.remove(self.gpkgSortie)
            raise ErreurConvertisseurGPKG('Traitement interompu!! Certaines couches dans la GDB contiennent des erreurs de géométries, veuillez corriger\n {}'
                                          .format(listErreurGeometrie))




    # def ajoutVues(self):
    #     """create view pee_classi_eco
    #         as select a.fid  as OGC_FID,, a.geom, a.geocode, a.origine, a.an_origine, a.perturb, a.an_perturb, a.type_couv, a.gr_ess, a.cl_dens, a.cl_haut, a.cl_age ,
    #             b.zone_veg, b.szone_veg, b.dom_bio, b.sdom_bio, b.reg_eco, b.sreg_eco, b.upays_reg, b.dis_eco
    #         from pee_ori a
    #         left join classi_eco_pee_ori b
    #         on a.geocode = b.geocode

    #         ******* le fid doit absolument s'apeller OGC_FID -> en maj sinon il y a un problème dans l'affichage QGIS...

    #     """
    #     listTable = self.serviceBd.getListTableBd()
    #     for uneTable in listTable:
    #         if uneTable not in ('pee_ori', 'pee_maj', 'perimetre_no_terri') and uneTable not in self.list_table_exclure_vue:
    #         # if uneTable not in ('pee_ori', 'pee_maj', 'perimetre_no_terri',  'meta_cmp_ori') and 'dendro_pee_tiges_dhp' not in uneTable:
    #             nomVue = "vue_peup_{}".format(uneTable)
    #             listChamp = self.serviceBd.getListchampTable(uneTable)

    #             if uneTable in self.listTable_1_1 :
    #                 print('vue cree sur table 1-1 {}'.format(uneTable))

    #                 sqlCreateView = "create view {0} as select a.fid as OGC_FID, a.geom, a.geocode, a.origine, a.an_origine, a.perturb, a.an_perturb, a.type_couv, a.gr_ess, a.cl_dens," \
    #                                 " a.cl_haut, a.cl_age, a.part_str, a.etagement, a.et_domi, a.couv_gaule, a.cl_pent, a.dep_sur, a.cl_drai, a.type_eco, a.co_ter, a.type_ter, a.strate," \
    #                                 "a.met_at_str, a.no_prg, a.ver_prg, a.superficie, ".format(nomVue)

    #                 for unChamp in listChamp:
    #                     if unChamp in ('fid', 'geocode', 'geom'):
    #                         continue

    #                     sqlCreateView += "b.{}, ".format(unChamp)

    #                 sqlCreateView = sqlCreateView[:-2]
    #                 sqlCreateView += " from pee_{0} a left join {1} b on a.geocode = b.geocode".format(self.ieqmType, uneTable )


    #             elif 'vgpkg_' not in uneTable :
    #                 print( 'vue cree sur table 1-n :{}'.format(uneTable))
    #                 # join table 1-n
    #                 # on inverse le left join (right join non supporté sqlite)
    #                 sqlCreateView = "create view {0} as select a.fid as OGC_FID, a.geocode, b.geom, ".format(nomVue)
    #                 for unChamp in listChamp:
    #                     if unChamp in ('fid', 'geocode'):
    #                         continue

    #                     sqlCreateView += "a.{}, ".format(unChamp)


    #                 sqlCreateView += " b.origine, b.an_origine, b.perturb, b.an_perturb, b.type_couv, b.gr_ess, b.cl_dens, b.cl_haut, b.cl_age, b.part_str, " \
    #                                  "b.etagement, b.et_domi, b.couv_gaule, b.cl_pent, b.dep_sur, b.cl_drai, b.type_eco, b.co_ter, b.type_ter, b.strate," \
    #                                  "b.met_at_str, b.no_prg, b.ver_prg, b.superficie " \
    #                                  "from {0} a " \
    #                                  "left join pee_{1} b " \
    #                                  "on a.geocode = b.geocode""".format(uneTable, self.ieqmType)
    #             else:
    #                 # table virtuel gpkg
    #                 continue


    #             ###########
    #             # on lance
    #             ##########
    #             self.serviceBd.executeRequete(sqlCreateView, commit=True)

    #             #########
    #             # ajoute content + geometry

    #             sqlInsertContents = """ insert into gpkg_contents select '{0}', data_type, '{0}', description, last_change, min_x, min_y, max_x, max_y, srs_id " \
    #                                 "from gpkg_contents where table_name = 'pee_{1}' """.format(nomVue, self.ieqmType)
    #             self.serviceBd.executeRequete(sqlInsertContents, commit=True)

    #             sqlInsertGeometry = """ insert into gpkg_geometry_columns select '{0}', column_name, geometry_type_name, srs_id, z, m " \
    #                                  "from gpkg_geometry_columns where table_name = 'pee_{1}' """.format(nomVue, self.ieqmType)
    #             self.serviceBd.executeRequete(sqlInsertGeometry, commit=True)

    # def corrigerGeometryInvalide(self, nomTab, nomColonneGeom):
    #     print('correction geom')
    #     self.serviceBd.ST_corrigerGeomNotValid(nomTable=nomTab, nomColonneGeom=nomColonneGeom)



if __name__ == '__main__':


    # convertisseur = ConvertisseurGPKG(gdbAConvert="E:/GPKG/carte_ori/11N/PRODUITS_IEQM_11N_10.gdb",
    #                              gpkgNomSortie="ieqm_11n_idem.gpkg",
    #                              pathGpkgStyle="",
    #                              pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
    #              )
    # convertisseur.conversion()

    # convertisseur = ConvertisseurGPKG(gdbAConvert="E:/GPKG/classi_eco/CLASSI_ECO_QC.gdb",
    #                              gpkgNomSortie="classi_eco.gpkg",
    #                              pathGpkgStyle="",
    #                              pathGdal="G:/OutilsProdDIF/modules_communs/gdal/gdal2.3.2",
    #              )
    # convertisseur.conversion()
    gdb = "D:/GPKG/probInvalidGeom/UPE_IEQM_REALISE_avecErreur.gdb"
    convertisseur = ConvertisseurGPKG(gdbAConvert=gdb,
                                 gpkgNomSortie="upeErr.gpkg",
                                 pathGpkgStyle="",
                                 pathGdal="")
    convertisseur.conversion()

    
    # convertisseur = ConvertisseurGPKG(gdbAConvert="C:/job/gpkg/11n/PRODUITS_IEQM_11N_10.gdb",
    #                              gpkgNomSortie="idem_11N.gpkg",
    #                              pathGpkgStyle="C:/job/GPKG/ieqm_styles.gpkg",
    #                              pathGdal="")
    # convertisseur.conversion()


    # except Exception as e:
    #     raise Exception('probleme avec la conversion en GPKG. Attention les chemin ne doivent pas contenir espace et carat accentué - {} '.format(e.message))

    # print('FIN')
